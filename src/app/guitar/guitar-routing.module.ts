import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { StringsComponent } from "./strings/strings.component";

const routes: Routes = [{ path: "", component: StringsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuitarRoutingModule {}
